({
    getNamedCredentialsHelper : function(component) {
        var action = component.get('c.getNamedCredentials');
        
        action.setCallback(this, function(actionStatus){
            var state = actionStatus.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(actionStatus.getReturnValue()) {
                    var listNamedCreds = actionStatus.getReturnValue();
                    component.set('v.namedCredentials', listNamedCreds);

                    if (listNamedCreds.length > 0) {
                        
                    component.set('v.selectednamedCredentials', listNamedCreds[0].DeveloperName);
                   this.getOpenBulkJobStatus(component);
                    }
                    else
                    {
                        component.set('v.checkNamedCredentials',true);
                    	
                        component.set('v.bulkJobOpen',true);
                    }
                     
                   
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        title : 'Something went wrong 1..', 
                        message : 'Please contact your system administrator.' ,
                        type : 'error',
                        mode : 'sticky'
                    }); 
                    showToast.fire(); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    initateRetrievalProcess : function(component) {
        var selectedComponents = component.get("v.selectedComponents");
        var selectednamedCredential = component.get("v.selectednamedCredentials") ;
        console.log(selectedComponents);
        console.log(selectedComponents.length);
        
        if(selectedComponents.length > 0){
            var action = component.get('c.initiateRetrival'); 
            action.setParams({
                selectedObjectMap: JSON.stringify(selectedComponents)  ,
                selectednamedCredentials: selectednamedCredential  
            });
            console.log('came here');

            action.setCallback(this, function(actionStatus){
                var state = actionStatus.getState(); // get the response state
                if(state == 'SUCCESS') {
                    console.log('success++');
               
                    if(actionStatus.getReturnValue()) {
                        console.log(' ---if  success-----');
                        component.set("v.bulkJobOpen",actionStatus.getReturnValue());
						 console.log(' ---if  success----- getDataRetriveStatus called');
                       
                        this.getDataRetrieveStatus(component);

                    } else {
                        var showToast = $A.get("e.force:showToast"); 
                        showToast.setParams({ 
                            title : 'Something went wrong..', 
                            message : 'Please contact your system administrator.' ,
                            type : 'error',
                            mode : 'sticky'
                        }); 
                        showToast.fire(); 
                    }
                }
            });

            $A.enqueueAction(action);
        } else {
            var showToast = $A.get("e.force:showToast"); 
            showToast.setParams({ 
                title : 'No Components selected.', 
                message : 'Please select the components to retrieve.' ,
                type : 'error',
                mode : 'sticky',
                messageTemplate : '{0}',
                messageTemplateData : ['Please select the components to retrieve.']
            }); 
            showToast.fire(); 
        }
    },
    transformAndAddData : function(component,event){
        var action = component.get('c.pushDataToSFDCCPQ');
        
        action.setCallback(this, function(actionStatus){
            var state = actionStatus.getState(); // get the response state
            component.set("v.spinner", false);
            if(state == 'SUCCESS') {
                if(actionStatus.getReturnValue())		{
                    console.log(actionStatus.getReturnValue());
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        title : 'Batch has been submitted..', 
                        message : 'Record are proccessing...Please wait..' ,
                        type : 'success',
                        mode : 'pester'
                    }); 
                    component.set('v.currentStep', '2');
                        
                    showToast.fire(); 
                }
                else{
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        title : 'Something went wrong..', 
                        message : 'Please contact your system administrator.' ,
                        type : 'error',
                        mode : 'sticky',
                        messageTemplate : '{0} - {1}',
                        messageTemplateData : ['Salesforce', 'Toasting Demo']
                    }); 
                    showToast.fire(); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    getOpenBulkJobStatus : function(component){
        var action = component.get('c.openBulkJobStatus');
        console.log('getOpenBulkJobStatus');
        action.setCallback(this, function(actionStatus){
            var state = actionStatus.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log('getOpenBulkJobStatus :'+actionStatus.getReturnValue());
                if (actionStatus.getReturnValue()) {
                    component.set('v.bulkJobOpen',actionStatus.getReturnValue());
                } else {
                    component.set('v.bulkJobOpen',actionStatus.getReturnValue());
                }
                this.getErrorDataUtil(component);
            }
        });
        $A.enqueueAction(action);
    },
    // params
    getDataRetrieveStatus : function name(component) {
        
       
            var interval = setInterval($A.getCallback(function () {
                //cmp
            var queuedJob = component.get("c.getDataRetrieveStatus");
            
            queuedJob.setCallback(this, function(jobStatusResponse){
                var state = queuedJob.getState();
                console.log('state'+state);
                if (state == "SUCCESS"){
                    
                    var jobStatus = jobStatusResponse.getReturnValue();
                    console.log('JobStatus'+jobStatus);
                    if(jobStatus == 'Completed') {
                        component.set('v.bulkJobOpen', false);
                        component.set('v.currentStep', '2');
                        clearInterval(interval);
                    } else {
                        component.set('v.bulkJobOpen', true);
                        component.set('v.currentStep', '1');
                    }
                }
            });

            $A.enqueueAction(queuedJob);
        }), 2000);
    },
    getErrorDataUtil : function(component) {
        var action = component.get('c.getErrorData');
        
        action.setCallback(this, function(actionStatus){
            var state = actionStatus.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(actionStatus.getReturnValue())		{
                    component.set('v.errorMessages',actionStatus.getReturnValue());
                } else {
                    var showToast = $A.get("e.force:showToast"); 
                    showToast.setParams({ 
                        title : 'Something went wrong 1..', 
                        message : 'Please contact your system administrator.' ,
                        type : 'error',
                        mode : 'sticky',
                        messageTemplate : '{0} - {1}',
                        messageTemplateData : ['Salesforce', 'Toasting Demo']
                    }); 
                    showToast.fire(); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    doRollback: function(component)
    {
         var action = component.get("c.cleanupData");
        action.setCallback(this,function(actionStatus){
            var state = actionStatus.getState(); // get the response state
            if(state == 'SUCCESS') {
                component.set('v.bulkJobOpen', false);
                var selectedComponents =  component.get("v.selectedComponents");
     			var options=component.get("v.options");
                    for(var i=0;i<options.length;i++){
           
            		options[i].checked=false;
           			selectedComponents.pop();
                    }
                component.set("v.selectedComponents",selectedComponents);
       			component.set("v.options", options);
      			component.set('v.currentStep', '1');
                var showToast = $A.get("e.force:showToast"); 
                    component.set("v.spinner", false);
                    showToast.setParams({ 
                        title : 'Rollback Successfull!!!', 
                        message : 'Record Rollbacked Successfully.' ,
                        type : 'success',
                        mode : 'pester'
                    }); 
                    showToast.fire(); 
                
            }
        });
        $A.enqueueAction(action);
        
    },
    selectAll :function(component,event) {
    
       var options=component.get("v.options");
       var selectedComponents =  component.get("v.selectedComponents");
      
        if( event.getSource().get("v.checked"))
        {
       for(var i=0;i<options.length;i++){
           
        	options[i].checked=true;
           	selectedComponents.push(options[i].value);
        		  
       }
       component.set("v.selectedComponents", selectedComponents);
             
        }
        else
        {
            for(var i=0;i<options.length;i++){
           
        	options[i].checked=false;
           	selectedComponents.pop();
        		  
       }
       component.set("v.selectedComponents",selectedComponents);
           
        	 }
     	component.set("v.selectedComponents",selectedComponents);
        
        component.set("v.options", options);
      
}
    
})