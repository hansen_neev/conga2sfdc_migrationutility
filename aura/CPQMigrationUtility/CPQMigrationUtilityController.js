({
    doInit : function(component,event,helper){
        component.set("v.spinner", true);
        helper.getNamedCredentialsHelper(component);
        component.set("v.spinner", false);
        var action = component.get('c.checkPermission');
        action.setCallback(this,function(response){
            
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log("From server:checkPermission " + response.getReturnValue());
                component.set('v.checkPermission',response.getReturnValue());  
            }  
            
        });        
        $A.enqueueAction(action);
    },
    addToList : function(component,event,helper){
        
    	 var capturedCheckboxName = event.getSource().get("v.value");
          var selectedComponents =  component.get("v.selectedComponents");
          if(selectedComponents.indexOf(capturedCheckboxName) > -1){
          	 selectedComponents.splice(selectedComponents.indexOf(capturedCheckboxName), 1);
          }
          else{
           	selectedComponents.push(capturedCheckboxName);
          }
          component.set("v.selectedComponents", selectedComponents);
          
    },
    retrieveData : function(component,event,helper){
        console.log('$$$selectednamedCredentials'+component.get("v.selectednamedCredentials"));
        helper.initateRetrievalProcess(component);
    },
    transformandadd :function(component,event,helper){
       	component.set("v.spinner", true);
        helper.transformAndAddData(component,event);
    },
    

    doRollback: function(component,event,helper)
    {
       helper.doRollback(component);
   },

    getErrorDataCnt: function(component,event,helper)
    {
        helper.getErrorDataUtil(component);
    },
    selectAll: function(component,event,helper){
     helper.selectAll(component,event);
    }
    
    
});