({
    
	insertCongaCPQMapping  : function(component) {
      
          
        
        var action = component.get('c.readStaticResources');
         action.setCallback(this,function(actionStatus){
             
            var state = actionStatus.getState(); // get the response state
            if(state == 'SUCCESS') {
                var showToast = $A.get("e.force:showToast"); 
              
        		showToast.setParams({ 
                        title : 'Records Inserted!!!', 
                        message : 'Record Inserted  Successfully.' ,
                        type : 'success',
                        mode : 'pester'
                    }); 
                    showToast.fire(); 
            }
         });
        
        
        $A.enqueueAction(action);
		
	}
})