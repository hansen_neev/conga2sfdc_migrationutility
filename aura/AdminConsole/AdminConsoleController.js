({
    insertCongaCPQMapping  : function(component,event,helper){
        helper.insertCongaCPQMapping(component);
        
    },
    doInit: function(component,event,helper){
        
        var action = component.get('c.validate');
        action.setCallback(this,function(response){
            
            var state = response.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log("From server: " + response.getReturnValue());
                component.set('v.staticresourcecount',response.getReturnValue());       
            }  
            
        });
        
        $A.enqueueAction(action);
    }
})