/**
 * @description       : 
 * @author            : Hansen Tech Team
 * @group             : 
 * @last modified on  : 03-12-2021
 * @last modified by  : Hansen Tech Team
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-03-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class GetApttusDataQueueable implements Queueable,Database.AllowsCallouts {
    String objectType;
    String pollingMode;
    String JobID;
    String bulkJobId;
    String nameCred;
    
    public GetApttusDataQueueable(String PollingMode, String sObjectType, String JobID,String bulkJobId,String nameCred){
        this.objectType = sObjectType;
        this.pollingMode = pollingMode;
        this.JobID = JobID;
        this.bulkJobId = bulkJobId;
        this.nameCred = nameCred;
     
    }
    
    public GetApttusDataQueueable(String PollingMode, String sObjectType, String JobID,String nameCred){
        this.objectType = sObjectType;
        this.pollingMode = pollingMode;
        this.JobID = JobID;
        this.nameCred = nameCred;
    }
    //Execute the batches for all the queuable jobs 
    public void execute(QueueableContext context) {
        try{     
            if(this.bulkJobID !=null){
                integer returnValue = new GetSourceData_MigrationUtility(this.objectType,this.pollingMode,this.JobID,this.bulkJobID,this.nameCred).initiateSync();
            }else{
                integer returnValue = new GetSourceData_MigrationUtility(this.objectType,this.pollingMode,this.JobID,this.nameCred).initiateSync();
            }
        }catch(Exception ex){
            Error_master__c objEM = new Error_Master__c(name = ex.getTypeName(), Error__c=ex.getStackTraceString()+' '+ex.getMessage());

            if (Schema.sObjectType.Error_master__c.fields.name.isUpdateable() && Schema.sObjectType.Error_master__c.fields.Error__c.isUpdateable()) {
                insert objEM;    
            }
        }    
    }
}