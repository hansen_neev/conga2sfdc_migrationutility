public class PAVFIeldResponseWrapper{
    public List<String> CustomFieldWrapper{get;set;}

    public class CustomFieldWrapper{
        public PicklistValuesWrapper picklistValues{get;set;}
    }
    
    public class PicklistValuesWrapper{
        public String value{get;set;}
    }
}