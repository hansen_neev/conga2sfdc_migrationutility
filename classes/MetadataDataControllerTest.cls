@isTest
private with sharing class MetadataDataControllerTest {
    @isTest
public static void testConstructResponseWithRootNode()
{
    ApexPages.currentPage().getParameters().put('node','root');
    Test.setMock(WebServiceMock.class, new  MetadataServiceDescribeMetadataMock());
       MetadataDataController metaDataController=new MetadataDataController();
        PageReference pr=metaDataController.constructResponse();
        System.assertEquals(pr,null);
       
}
    @isTest
    public static void testConstructResponseWithoutRootNode()
{
    ApexPages.currentPage().getParameters().put('node','xvt');
    
        Test.setMock(WebServiceMock.class, new  MetadataServiceListMetadataMock());
       MetadataDataController metaDataController=new MetadataDataController();
        PageReference pr=metaDataController.constructResponse();
        System.assertEquals(pr,null);
       
    
}
    @isTest
     public static void testConstructResponseWithException()
{
    MetadataDataController metaDataController=new MetadataDataController();
        PageReference pr=metaDataController.constructResponse();
        System.assertEquals(pr,null);
}
}