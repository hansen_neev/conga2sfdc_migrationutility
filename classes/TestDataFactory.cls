/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-10-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-10-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public with sharing class TestDataFactory {
    //For DeleteAll
    public static SBQQ__ConfigurationAttribute__c getConfigurationAttribute() {
        SBQQ__ConfigurationAttribute__c oSP = new SBQQ__ConfigurationAttribute__c();
         return oSP;
    }
    public static Pricebook2 getPricebook2() {
        Pricebook2 oSP = new Pricebook2();
          oSP.name='pricebook';
          osp.ExternalId__c='67';
         return oSP;
    }
 	public static SBQQ__ProductOption__c getProductOption() {
        SBQQ__ProductOption__c oSP = new SBQQ__ProductOption__c();
     	oSP.SBQQ__Number__c=2;
        return oSP;
    }
 	public static SBQQ__ProductFeature__c getProductFeature() {
        SBQQ__ProductFeature__c oSP = new SBQQ__ProductFeature__c();
      	oSP.SBQQ__Number__c=2;
        return oSP;
    }
    public static Product2 getProduct2() {
        Product2 oSP = new Product2();
      	return oSP;
    }
    //For CleanupUtility

    public static Stage_Product__c getStageProduct() {
        Stage_Product__c oSP = new Stage_Product__c();
        oSP.Product2_Name__c = 'Product 1';
        return oSP;
    }
    public static Stage_Pricing__c getStagePricing() {
        Stage_Pricing__c oSP = new Stage_Pricing__c();
       
        return oSP;
    }
 	public static Stage_Attribute__c getStageAttribute() {
        Stage_Attribute__c oSP = new Stage_Attribute__c();
       	return oSP;
    }
    public static Error_Master__c getErrorMaster() {
        Error_Master__c oSP = new Error_Master__c();
       	return oSP;
    }
    public static Bulk_Jobs_Status__c getBulkJobStatus() {
        Bulk_Jobs_Status__c oSP = new Bulk_Jobs_Status__c();
       	return oSP;
    }
}