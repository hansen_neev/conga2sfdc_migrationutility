public class AttributeWrapper {
    public String type{get;set;}
    public String referenceId{get;set;}
    
    public AttributeWrapper(String objType,String refId){
        this.type = objType;
        this.referenceId = refId;
    }
}