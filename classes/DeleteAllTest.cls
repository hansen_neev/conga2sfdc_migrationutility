/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-22-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private class DeleteAllTest {
    @isTest static void testDelete_data(){  
        SBQQ__ConfigurationAttribute__c sc=TestDataFactory.getConfigurationAttribute();
        insert sc;
        Pricebook2 pc=TestDataFactory.getPricebook2();
        insert pc;
        SBQQ__ProductOption__c po=TestDataFactory.getProductOption();
        insert po;
        SBQQ__ProductFeature__c pf=TestDataFactory.getProductFeature();
        insert pf;
        DeleteAll.delete_data();
        List<SBQQ__ConfigurationAttribute__c> configuration= [select id,name from SBQQ__ConfigurationAttribute__c   where createddate = today or ExternalId__c !=null];
        System.assertEquals(configuration.isEmpty(), true);
       List<Pricebook2>  pricebook2= [select id,name from Pricebook2  where createddate = today ];
        System.assertEquals(pricebook2.isEmpty(), true);
       List<SBQQ__ProductOption__c> productOption=[select id,name from SBQQ__ProductOption__c WHERE createddate = today or ExternalId__c  !=null];
        System.assertEquals(productOption.isEmpty(), true);
       
        List<SBQQ__ProductFeature__c> productFeature=[select id,name from SBQQ__ProductFeature__c WHERE createddate = today or ExternalId__c  !=null];
       System.assertEquals(productFeature.isEmpty(), true);
       
        List<Product2> product2=[select id,name from product2 WHERE createddate = today or External_Id__c !=null];
        System.assertEquals(product2.isEmpty(), true);
    }


    @isTest static void testRetrieve_data(){  
        DeleteAll.retrieveData();
    }
}