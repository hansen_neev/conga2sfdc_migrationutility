@isTest
public class UtilityControllerTest {
    @isTest 
    public static void utilityControllerTestMethod()
    {
        UtilityController utilityController=new UtilityController();
        utilityController.initiate();
        PageReference p=utilityController.getPageMessage();
        System.assertEquals(p,null);
        List<String> message=utilityController.getMessage();
        System.assertEquals(message,null);
    }
    @isTest
    public static void utilityControllerTestMethodWithSizeGreaterThanZero()
    {
       Bulk_Jobs_Status__c bjs=new Bulk_Jobs_Status__c();
        insert bjs;
        UtilityController utilityController=new UtilityController();
        utilityController.initiate();
        PageReference p=utilityController.getPageMessage();
        System.assertEquals(p,null);
        List<String> message=utilityController.getMessage();
        System.assertNotEquals(message,null);
    }

}