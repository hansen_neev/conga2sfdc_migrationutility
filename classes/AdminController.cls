public class AdminController {
    @AuraEnabled
    public static void readStaticResources(){
        try{
           
            InsertCongaFieldMapping.deleteAllData();
            Map<String,StaticResource>staticResourceMap=InsertCongaFieldMapping.loadStaticResources();
            InsertCongaFieldMapping.insertSourceToStaging(staticResourceMap);
            InsertCongaFieldMapping.insertStagingToCpq(staticResourceMap);
        }catch(Exception utilityException){
            System.debug('Exception Occurred--> '+utilityException.getMessage()+'   '+utilityException.getStackTraceString());
            Error_master__c objEM = new Error_Master__c(name = utilityException.getTypeName(), Error__c=utilityException.getStackTraceString());
            if (Schema.sObjectType.Error_master__c.fields.name.isUpdateable() && Schema.sObjectType.Error_master__c.fields.Error__c.isUpdateable()) {
                insert objEM;    
            }
        }
    }
    @AuraEnabled
    public static boolean validate()
    {        
        List<Source_to_Staging__c> sourcestagging = [select Id from Source_to_Staging__c];        
        List<Staging_to_CPQ__c> Staggingcpq = [select Id from Staging_to_CPQ__c];
        if(Staggingcpq.size()>0 && sourcestagging.size()>0){
            return true;
        }
        else {return false;}        
    }
    
}