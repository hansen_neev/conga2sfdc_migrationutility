/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-17-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-08-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/

public class SourceData_to_Staging_UtilityHelper {
//initiating request for queryJob
    public static String initiateBulkQueryJob(String ObjectName,String query,string namecred){  
        String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String jsondata = '{\"operation\": \"query\",\"query\": \"'+query+'\"}';
        String response = performAction('','callout:'+namecred ,'POST','services/data/'+CurrentAPIVersion+'/jobs/query',jsondata,'application/json');
        if(response.length()>0){
        String jobId = response.substring(response.indexOf('id')+5,response.indexOf('id')+20);    
            
        return jobId;
        }
        else{
            return '';
        }
        
    }
    // invokes when the job is completed
     public static boolean pollTillJobComplete(String jobId,string namecred){  
        String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String response = performAction('','callout:'+namecred,'GET','services/data/'+CurrentAPIVersion+'/jobs/query/'+jobId,'','application/json');
    
        if(response.indexOf('JobComplete') > -1){
            return true;
        }
        else{
            return pollTillJobComplete(jobId,namecred); 
        }
        }
    //Method for getting JobResponse
     public static String getJobResponse(String jobId,string namecred){
         String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String response = performAction('','callout:'+namecred,'GET','services/data/'+CurrentAPIVersion+'/jobs/query/'+jobId+'/results','','application/json');
        return response;
    }
    //Methord for getting reformed response
    public static String getReformedResponse(Map<String,String> objectMap,String csvdata){
        
        for(String keyValue : objectMap.keySet()){
            csvdata = csvdata.replaceFirst('"'+keyValue+'"', '"'+objectMap.get(keyValue)+'"');
        }
        return csvdata;
    }
    //
    public static boolean initiatePush(String csvdata){
        
        return false;
    }
    // Method for creating Job
    public static String createJobForInsert(String objectName){
        String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String jsondata = '{\"operation\":\"insert\",\"object\":\"'+objectName+'\",\"contentType\":\"CSV\",\"lineEnding\":\"LF\"}';
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String response = performAction('Bearer '+Userinfo.getSessionId(),baseUrl,'POST','services/data/'+CurrentAPIVersion+'/jobs/ingest',jsondata,'application/json');
        String responseJobId = response.substring(7,25);
        return responseJobId;
    }
    //Method for pushing data into job
    public static boolean pushDataToJob(String jobId,String reFormedResponse){
        String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String response = performAction('Bearer '+Userinfo.getSessionId(),baseUrl,'PUT','services/data/'+CurrentAPIVersion+'/jobs/ingest/'+jobId+'/batches',reFormedResponse,'text/csv');
        return true;
    }
    //Method for closing Job
    public static boolean closeJob(String jobId){
        String CurrentAPIVersion = System.Label.CurrentAPIVersion;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String jsondata = '{\"state\" : \"UploadComplete\"}';
        String response = performAction('Bearer '+Userinfo.getSessionId(),baseUrl,'PATCH','services/data/'+CurrentAPIVersion+'/jobs/ingest/'+jobId,jsondata,'application/json');
        return true;
        
    }
    //Method for sending request and getting response for queryJob
    public static String performAction(String authorization,String namedCredentials,String method,String endPoint,String jsondata,String contentType){
       
        String finalEndPoint = namedCredentials+'/'+endPoint;
        Http httpConnect = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setHeader('Content-Type',contentType);
        httpRequest.setHeader('accept','application/json');
        httpRequest.setMethod(method);
        httpRequest.setEndpoint(finalEndPoint);
        if(jsondata != ''){
            httpRequest.setBody(jsondata);    
        }
        if(authorization != ''){
            httpRequest.setHeader('Authorization', authorization);
        }
		System.debug(httpRequest);
        HttpResponse response;
        try {
            response = httpConnect.send(httpRequest);
             while (response.getStatusCode() == 302) {
                httpRequest.setEndpoint(response.getHeader('Location'));
                response = new Http().send(httpRequest);
            }
        } catch (Exception ex) {
            system.debug('###'+ ex);
            throw ex;
        }
        return response.getBody();       
    }
}