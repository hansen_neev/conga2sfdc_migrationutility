global class myBatchClass implements 
    Database.Batchable<sObject>, Database.Stateful {    
    String inputData='';
    global myBatchClass(String csvdata){
        csvdata = inputData;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //return null;     
        return Database.getQueryLocator('SELECT ID FROM SBQQ__SummaryVariable__c');
    }
    global void execute(Database.BatchableContext bc, List<SBQQ__SummaryVariable__c> scope){
    
        System.debug('--->Execute<---');
    }    
    global void finish(Database.BatchableContext bc){       
        System.debug('Finally Its finished');        
    }    
}