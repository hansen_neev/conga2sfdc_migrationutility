
public  class MetadataServiceDeployOptionsMock implements WebServiceMock {
    public void doInvoke(
        Object stub, Object request, Map<String, Object> response,
        String endpoint, String soapAction, String requestName,
        String responseNS, String responseName, String responseType) 
        {
            MetadataService.AsyncResult result=new MetadataService.AsyncResult();
            result.done=true;
            result.id='someId';
            result.message='somemessage';
            
            result.state='';
            result.statusCode='200';
            
            
            MetadataService.deployResponse_element res=new MetadataService.deployResponse_element();
            res.result=result;
            response.put('response_x',res);
        }
     
}
