global class Hansen_ProductMigrationBatch implements Database.Batchable<sObject>, Database.Stateful ,Database.AllowsCallouts {
    Map<String,String> productFieldMap;
    public String globalEndPoint = 'services/data/v49.0/composite/tree/';
    List<APIResponse> responsesProductMigration = new List<APIResponse>();
    List<ResponseData> responsesData = new List<ResponseData>();
    
    public Hansen_ProductMigrationBatch(){
        
        productFieldMap = new Map<String,String>();
        productFieldMap.put('Product_Name__c','Name');
        productFieldMap.put('Is_Active__c','IsActive');
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id,Is_Active__c,Product_Name__c from ProductToTransform__c WHERE Is_Migrated__c=false limit 10');
    }

    global void execute(Database.BatchableContext bc, List<ProductToTransform__c> productList){
        List<ID> liProductID = new List<Id>();
        List<ProductMigration> productMigrationwrap = new List<ProductMigration>();
        for(ProductToTransform__c productSObj : productList){
            AttributeWrapper awrap = new AttributeWrapper('Product2',productSObj.ID);
            ProductMigration productWrapper = new ProductMigration(awrap,productSObj);
            productMigrationwrap.add(productWrapper);
        }
        
        MigrationWrapper mgw = new MigrationWrapper(productMigrationwrap);
        System.debug('mgw--> '+mgw);
        String jsondata = JSON.serialize(mgw);
        for(String fieldId : productFieldMap.keySet()){
            if(jsondata.indexOf(fieldId) > -1){
                jsondata = jsondata.replaceAll(fieldId, productFieldMap.get(fieldId));
            }
        }
        
        System.debug('jsondata--> '+jsondata);
        APIResponse responseData = migrateData('Product2',jsondata);
        responsesProductMigration.add(responseData);
        for(ResponseData objRData : responseData.results){
            responsesData.add(objRData);
        }
    }
    
    
    global void finish(Database.BatchableContext bc){
        List<ProductToTransform__c> liProductsUpdate = new List<ProductToTransform__c>();
        APIResponse CombinedResponse = combineResponse(responsesProductMigration);
    
        if(CombinedResponse.hasErrors){
            //Error Handling Object Entry
        }else{
            for(ResponseData objRP : CombinedResponse.results){
                if(objRP.referenceId !=null && objRP.Id !=null){
                    liProductsUpdate.add(new ProductToTransform__c(Id = objRP.referenceId,Is_Migrated__c = true));
                }    
            }
        }
        
        if(liProductsUpdate.size() >0){
            update liProductsUpdate;
        }
        
        
        List<ProductToTransform__c> liNonProcessedProducts = [Select id from ProductToTransform__c WHERE Is_Migrated__c = false];
        if(liNonProcessedProducts.size() >0){
            System.debug('In Update--> '+liNonProcessedProducts);
            Database.executeBatch(new Hansen_ProductMigrationBatch() , 10);
        }else{
            //here call another batch which will process data of next object 
            //Database.executeBatch(new Hansen_PricebookandEntryMigrationBatch(productMigrationResponse) , 20);
        }
    
    }

    private APIResponse combineResponse(List<APIResponse> responsesProductMigrationParam){
        APIResponse returnResponse = new APIResponse();
        returnResponse.hasErrors = false;
        returnResponse.results = new List<ResponseData>();
        
        for(APIResponse objResponse : responsesProductMigrationParam){
            if(objResponse.hasErrors){
                returnResponse.hasErrors = true;        
            }
        }
        returnResponse.results = responsesData;
        System.debug('final response--> '+returnResponse);
        return returnResponse;
    }
    
    public APIResponse migrateData(String objectName,String jsonData){
        String endPoint = globalEndPoint + objectName;
        Http h2 = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('accept','application/json');
      
        req1.setMethod('POST');
        req1.setEndpoint('callout:Viraj_CPQ_ORG/'+endPoint);
        req1.setBody(jsondata);
        HttpResponse res1 = h2.send(req1);
        system.debug('My response--> '+res1);
        APIResponse responseData = (APIResponse) JSON.deserialize(res1.getBody(),APIResponse.class);
        
        return responseData;
    }    

}