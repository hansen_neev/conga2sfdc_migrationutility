public class UtilityController{
    public List<String> liMessages{get;set;}
    public void initiate(){
        liMessages = new List<String>();
    }
    
    public PageReference getPageMessage() {
        liMessages = getMessage();
        return null;
    }
    public List<String> getMessage(){
        List<Bulk_Jobs_Status__c> liJobs = [Select Id, Name,Bulk_Job_ID__c, Status__c from Bulk_Jobs_Status__c ORDER BY lastmodifieddate ASC];
        List<String> liMessages = new List<String>();
        if(liJobs.size() >0){
            for(Bulk_Jobs_Status__c objJob : liJobs){
                liMessages.add(objJob.Name + ' --> '+ objJob.Status__c);
            }
            return liMessages;
        } 
        return null;
    }
}