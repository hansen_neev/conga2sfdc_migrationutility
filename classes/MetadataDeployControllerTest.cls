@isTest
public inherited sharing class MetadataDeployControllerTest {
    @isTest
    public static void testGetMethods()
    {
        Test.startTest();
        MetadataDeployController mdc=new MetadataDeployController();
        System.assertEquals('public class HelloWorld' + 
        '{' + 
            'public static void helloWorld()' +
            '{' + 
                'System.debug(\' Hello World\');' +
            '}' +
        '}', mdc.getHelloWorld());

        System.assertEquals('<?xml version="1.0" encoding="UTF-8"?>' +
        '<ApexClass xmlns="http://soap.sforce.com/2006/04/metadata">' +
            '<apiVersion>28.0</apiVersion>' + 
            '<status>Active</status>' +
        '</ApexClass>', mdc.getHelloWorldMetadata());

        System.assertEquals('<?xml version="1.0" encoding="UTF-8"?>' + 
        '<Package xmlns="http://soap.sforce.com/2006/04/metadata">' + 
            '<types>' + 
                '<members>HelloWorld</members>' +
                '<name>ApexClass</name>' + 
            '</types>' + 
            '<version>26.0</version>' + 
        '</Package>', mdc.getPackageXml());
        Test.stopTest();
    }

    @isTest
    public static void deployZipTest()
    {
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MetadataServiceDeployOptionsMock());
        MetadataDeployController mdc=new MetadataDeployController();
        mdc.deployZip();
        Test.setMock(WebServiceMock.class, new MetadataServiceDeployResultMock());
        mdc.checkAsyncRequest();
        Test.stopTest();
    }


}
