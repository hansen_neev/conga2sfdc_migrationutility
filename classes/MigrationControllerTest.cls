@isTest
public with sharing  class MigrationControllerTest
{
     @TestSetup
    static void makeData(){
        List<Stage_Product__c> listObjects = new List<Stage_Product__c>();
        Stage_Product__c record1 = new Stage_Product__c();
        record1.Product2_Active__c=true;
        record1.Product2_Name__c='SomeName';
        listObjects.add(record1);
        insert listObjects;

        PriceBook2 pb2=new PriceBook2();
        pb2.IsActive=true;
        pb2.Name='SomeName';
        List<PriceBook2> pbList=new List<PriceBook2>();
        pbList.add(pb2);
        insert pbList;

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
            );
            Update standardPricebook;
    } 
    @IsTest
    public static void getNamedCredentialsTest()
    {
        Test.startTest();
        List<NamedCredential> namedCradentials=MigrationController.getNamedCredentials();
        System.debug(namedCradentials);
        Test.stopTest();
    }

    @isTest
    public static void initiateRetrivalTest()
    {
        Test.setMock(HttpCalloutMock.class, new MigrationUtilityTestHttpMock());
      
        Boolean res=MigrationController.initiateRetrival('["Product2"]','devApttusApi'); 
        System.assertEquals(true, res);
    }

    @isTest
    public static void initiateRetrivalTest2()
    {
           Test.setMock(HttpCalloutMock.class, new MigrationUtilityTestHttpMock());
      
        Boolean res=MigrationController.initiateRetrival('["ProductAttribute"]','devApttusApi'); 
        System.assertEquals(true, res);
    }

    @isTest
    public static void openBulkJobStatus()
    {
        Test.startTest();
        Boolean res=MigrationController.openBulkJobStatus();
        Test.stopTest();
    }

    @isTest
    public static void checkQueuedJobsTest()
    {
        Test.startTest();
        List<AsyncApexJob> jobs=MigrationController.checkQueuedJobs();
        Test.stopTest();
    }

    
    @isTest
    public static void pushDataToSFDCCPQTest()
    {
        Test.startTest();
        Boolean res=MigrationController.pushDataToSFDCCPQ();
        Test.stopTest();
    }
    @isTest
    public static void TestMigrationUtilityWithCloseBulkStatus()
    {

     Bulk_Jobs_Status__c bulkJobStatus = new Bulk_Jobs_Status__c();
      bulkJobStatus.Status__c = 'Open';
   
        insert bulkJobStatus;
         Test.startTest();
        Boolean res=MigrationController.openBulkJobStatus();
        Test.stopTest();
   
    }       
 
    @isTest
    public static void TestMigrationUtility()
    {
   boolean m= MigrationController.cleanupData();    
        System.assertEquals(m,true);
        MigrationController.getErrorData();
    }
    
    @isTest
    public static void TestRetrieveData()
    {
        Bulk_Jobs_Status__c bcc=new Bulk_Jobs_Status__c();
        insert bcc;
  		MigrationController.getDataRetrieveStatus();
    }
    
    
   }