public class MetadataServiceDescribeMetadataMock implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			
            if(requestName=='describeMetadata')
            {
		    MetadataService.DescribeMetadataResult result=new MetadataService.DescribeMetadataResult();
            result.organizationNamespace='ABC';
            result.partialSaveAllowed=true;
            result.testRequired=true;
            MetadataService.DescribeMetadataObject obj=new MetadataService.DescribeMetadataObject();
            obj.childXmlNames=new List<String>{''};
            obj.directoryName='';
            obj.inFolder=true;
            obj.metaFile=true;
            obj.suffix='';
            obj.xmlName='';
            MetadataService.DescribeMetadataObject[] objList=new List<MetadataService.DescribeMetadataObject>();
            objList.add(obj);
            result.metadataObjects=objList;
            MetadataService.describeMetadataResponse_element res=new MetadataService.describeMetadataResponse_element();
            res.result=result;
            response.put('response_x',res);
            }
            else if(requestName=='listMetadata')
            {
                MetadataService.FileProperties result=new MetadataService.FileProperties();
                result.createdById='ABC';
                result.createdByName='someName';
                result.createdDate=Datetime.newInstance(1960, 2, 17);
                result.fileName='';
                result.fullName='';
                result.id='';
                result.lastModifiedById='';
                result.lastModifiedByName='';
                result.lastModifiedDate=Datetime.newInstance(1960, 2, 17);
                result.manageableState='';
                result.namespacePrefix='';
                result.type_x='';
    
                List<MetadataService.FileProperties> fileList=new List<MetadataService.FileProperties>();
                fileList.add(result);
    
                MetadataService.listMetadataResponse_element res=new MetadataService.listMetadataResponse_element();
                res.result=fileList;
                response.put('response_x',res);
            }
		}
	}