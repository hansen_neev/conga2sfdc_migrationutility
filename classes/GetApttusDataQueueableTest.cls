@isTest
public class GetApttusDataQueueableTest {
    @isTest
    public static void getApttusDataQueueableTestMethod()
    {
        Test.startTest();        
        System.enqueueJob(new GetApttusDataQueueable('','','','3','devApttusApi'));
        Test.stopTest();}
    @isTest
    public static void getApttusDataQueueableTestMethodWithNullBulkId()
    {
   
        Test.startTest();        
        System.enqueueJob(new GetApttusDataQueueable('','','',null,null));
        Test.stopTest();   
}
   @isTest
    public static void getApttusDataQueueableTestMethodWithException()
    {
    
        Test.startTest();        
        System.enqueueJob(new GetApttusDataQueueable('','','',''));
        Test.stopTest();
    }
}