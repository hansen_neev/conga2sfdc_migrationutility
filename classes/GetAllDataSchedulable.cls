global class GetAllDataSchedulable implements Schedulable {
    String JobID;
    String objectType;
    public GetAllDataSchedulable(String JobID, String objectType){
        this.JobID = JobID;
        this.objectType = objectType;
    }
    global void execute(SchedulableContext sc) {
        CronTrigger ct = [SELECT TimesTriggered, NextFireTime FROM CronTrigger WHERE id=:sc.getTriggerId()];
        System.abortJob(ct.Id);
        
        System.enqueueJob(new GetApttusDataQueueable('true',this.objectType,this.JobID));
   }
   }