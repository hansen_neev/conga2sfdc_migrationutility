public  class MetadataServiceDeployResultMock implements WebServiceMock{
    public  void doInvoke(
        Object stub, Object request, Map<String, Object> response,
        String endpoint, String soapAction, String requestName,
        String responseNS, String responseName, String responseType) 
         {
        MetadataService.DeployResult result=new MetadataService.DeployResult();
        result.canceledBy='abc';
        result.canceledByName='noName';
        result.checkOnly=true;
        result.completedDate=Datetime.newInstance(1960, 2, 17);
        result.createdBy='by';
        result.createdByName='byName';
        result.createdDate=Datetime.newInstance(1960, 2, 17);

        MetadataService.DeployDetails dd=new MetadataService.DeployDetails();
        
        MetadataService.DeployMessage[] dmList=new List<MetadataService.DeployMessage>();
        MetadataService.DeployMessage dm=new MetadataService.DeployMessage();
        dm.changed=true;
        dm.columnNumber=5;
        dm.componentType='comType';
        dm.created=true;
        dm.createdDate=Datetime.newInstance(1960, 2, 17);
        dm.deleted=false;
        dm.fileName='someNme';
        dm.fullName='someNme';
        dm.id='111';
        dm.lineNumber=5;
        dm.problem='';
        dm.problemType='';
        dm.success=true;

        dmList.add(dm);

        MetadataService.RetrieveResult rr=new MetadataService.RetrieveResult();
        rr.done=true;
        rr.errorMessage='';
        rr.errorStatusCode='';
        MetadataService.FileProperties[] fp=new List<MetadataService.FileProperties>();
        rr.fileProperties=fp;
        rr.id='111';
        rr.messages=new List<MetadataService.RetrieveMessage>();
        rr.status='successs';
        rr.success=true;
        rr.zipFile='filename';

        MetadataService.RunTestsResult rtr=new MetadataService.RunTestsResult();
        rtr.apexLogId='';
        rtr.codeCoverage=new List<MetadataService.CodeCoverageResult>();
        rtr.codeCoverageWarnings=new List<MetadataService.CodeCoverageWarning>();
        rtr.failures=new List<MetadataService.RunTestFailure>();
        rtr.numFailures=9;
        rtr.numTestsRun=3;
        rtr.successes=new List<MetadataService.RunTestSuccess>();
        rtr.totalTime=2.0;

        dd.componentFailures=dmList;
        dd.componentSuccesses=dmList;
        dd.retrieveResult=rr;
        dd.runTestResult=rtr;
        
        result.details=dd;
        
        result.done=true;
        result.errorMessage='noerror';
        result.errorStatusCode='200';
        result.id='id';
        result.ignoreWarnings=true;
        result.lastModifiedDate=Datetime.newInstance(1960, 2, 17);
        result.numberComponentErrors=1;
        result.numberComponentsDeployed=2;
        result.numberComponentsTotal=5;
        result.numberTestErrors=0;
        result.numberTestsCompleted=5;
        result.numberTestsTotal=2;
        result.rollbackOnError=true;
        result.runTestsEnabled=true;
        result.startDate=Datetime.newInstance(1960, 2, 17);
        result.stateDetail='state';
        result.status='success';
        result.success=true;

        MetadataService.checkDeployStatusResponse_element res=new MetadataService.checkDeployStatusResponse_element();
        res.result=result;
        response.put('response_x',res);
    }
}
