public class MetadataAsyncRequestTestMock implements WebServiceMock {
 		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
            MetadataService.RetrieveResult retrieveResult=new MetadataService.RetrieveResult();
              retrieveResult.done=true;
       		 retrieveResult.errorMessage='hello Error';
      		 retrieveResult.errorStatusCode='32';
            MetadataService.FileProperties result=new MetadataService.FileProperties();
            result.createdById='ABC';
            result.createdByName='someName';
            result.createdDate=Datetime.newInstance(1960, 2, 17);
            result.fileName='';
            result.fullName='';
            result.id='';
            result.lastModifiedById='';
            result.lastModifiedByName='';
            result.lastModifiedDate=Datetime.newInstance(1960, 2, 17);
            result.manageableState='';
            result.namespacePrefix='';
            result.type_x='';

            List<MetadataService.FileProperties> fileList=new List<MetadataService.FileProperties>();
            fileList.add(result);

        retrieveResult.fileProperties=fileList;
        retrieveResult.id='32';
          
        List<MetadataService.RetrieveMessage> messageList=new List<MetadataService.RetrieveMessage>();
       retrieveResult.messages=messageList;
       retrieveResult.status='Succeeded';
       retrieveResult.success=true;
        retrieveResult.zipFile='hh';
         MetadataService.checkRetrieveStatusResponse_element res=new MetadataService.checkRetrieveStatusResponse_element();
          res.result=retrieveResult;
          response.put('response_x',res);
     
	}

}