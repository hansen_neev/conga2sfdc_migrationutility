public class MetadataServiceListMetadataMock implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			
		    MetadataService.FileProperties result=new MetadataService.FileProperties();
            result.createdById='ABC';
            result.createdByName='someName';
            result.createdDate=Datetime.newInstance(1960, 2, 17);
            result.fileName='';
            result.fullName='';
            result.id='';
            result.lastModifiedById='';
            result.lastModifiedByName='';
            result.lastModifiedDate=Datetime.newInstance(1960, 2, 17);
            result.manageableState='';
            result.namespacePrefix='';
            result.type_x='';

            List<MetadataService.FileProperties> fileList=new List<MetadataService.FileProperties>();
            fileList.add(result);

            MetadataService.listMetadataResponse_element res=new MetadataService.listMetadataResponse_element();
            res.result=fileList;
            response.put('response_x',res);
		}
	}