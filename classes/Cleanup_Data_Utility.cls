public class Cleanup_Data_Utility{
    @AuraEnabled
    // delete all staging data
    public static void deleteStageData(){
        List<Stage_Product__c> liStageProduct = [select Id from Stage_Product__c ];
        if (Schema.sObjectType.Stage_Product__c.isDeletable()) {
            delete liStageProduct;
        }
        
        List<Stage_Pricing__c> liStagePricing = [Select id from Stage_Pricing__c];
        if (Schema.sObjectType.Stage_Pricing__c.isDeletable()) {
      	delete liStagePricing;
        }
        
        List<Stage_Attribute__c> liStageAttribute = [Select Id from Stage_Attribute__c];
       if (Schema.sObjectType.Stage_Attribute__c.isDeletable()) {
           delete liStageAttribute;
       }
        
        List<Error_Master__c> liErrorMaster = [Select Id from Error_Master__C];
     	if (Schema.sObjectType.Error_Master__c.isDeletable()) {
            delete liErrorMaster;
        }
        List<Bulk_Jobs_Status__c> liBulkStatus = [select Id from Bulk_Jobs_Status__c ];
        if (Schema.sObjectType.Bulk_Jobs_Status__c.isDeletable()) {
         delete liBulkStatus;    
        }
    }
    
    //delete all the target data
     public static void deleteTargetData(){
        List<Pricebook2> liPricebook = [select Id from Pricebook2 WHERE ExternalId__c !=null];
        if (Schema.sObjectType.Pricebook2.isDeletable()) {
        
        delete liPricebook;
        }
         
        List<PricebookEntry> liPriceBookEntry = [SELECT ID from PricebookEntry where pricebook2.isStandard = false or Product2.External_Id__c != null];
        if (Schema.sObjectType.PricebookEntry.isDeletable()) {
        
         delete liPriceBookEntry;
        }
         
        List<Product2> liProducts = [select Id from product2 WHERE External_Id__c !=null];
         if (Schema.sObjectType.Product2.isDeletable()) {
        delete liProducts;
         }
        List<SBQQ__ProductFeature__c> liPFeature = [select Id from SBQQ__ProductFeature__c  WHERE ExternalId__c !=null];
         if (Schema.sObjectType.SBQQ__ProductFeature__c.isDeletable()) {
        	delete liPFeature;
         } 
        List<SBQQ__ProductOption__c> liPOption = [select Id from SBQQ__ProductOption__c WHERE ExternalId__c !=null];
         if (Schema.sObjectType.SBQQ__ProductOption__c.isDeletable()) {
        delete liPOption ;
         }
        List<SBQQ__AttributeSet__c> liAttributeSet = [select Id from SBQQ__AttributeSet__c  WHERE ExternalId__c !=null];
        if (Schema.sObjectType.SBQQ__AttributeSet__c.isDeletable()) {
         delete liAttributeSet;
        }
        List<SBQQ__ProductAttributeSet__c> liProductAttributeSet = [select Id from SBQQ__ProductAttributeSet__c];
         if (Schema.sObjectType.SBQQ__ProductAttributeSet__c.isDeletable()) {
       		delete liProductAttributeSet;
         }
        List<SBQQ__ProductAttribute__c> liPA = [select Id from SBQQ__ProductAttribute__c ];
         if (Schema.sObjectType.SBQQ__ProductAttribute__c.isDeletable()) {
        	delete liPA;  
         }
        
        List<SBQQ__ConfigurationAttribute__c> liCA = [select Id from SBQQ__ConfigurationAttribute__c WHERE ExternalId__c !=null];
        if (Schema.sObjectType.SBQQ__ConfigurationAttribute__c.isDeletable()) {
            
             delete liCA;
            
            }
     }
    //delete both stage and target data
    public static void deleteAlldata(){
        deleteStageData();
        deleteTargetData();
    }
}