@isTest
public inherited sharing class APIResponseTest {
    @isTest
    public static void test()
    {
        APIResponse apiRes=new APIResponse();
        apiRes.hasErrors=false;
        List<ResponseData> ls=new List<ResponseData>();
        ResponseData rd=new ResponseData();
        rd.referenceId='refid';
        rd.id='id';
        ls.add(rd);
        apiRes.results=ls;
        System.assertEquals(false, apiRes.hasErrors);
        System.assertEquals('id', apiRes.results[0].id);
    }
}