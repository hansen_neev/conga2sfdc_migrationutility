/**
* @description       : 
* @author            : ChangeMeIn@UserSettingsUnder.SFDoc
* @group             : 
* @last modified on  : 03-10-2021
* @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
* Modifications Log 
* Ver   Date         Author                               Modification
* 1.0   09-14-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class MigrationController {
    
    @AuraEnabled
    //Method for checking Salesforce CPQ Admin Permission
    public static boolean checkPermission() {
        List<PermissionSetAssignment> permissioncheck= [SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.ProfileId, PermissionSet.Profile.Name, AssigneeId, Assignee.Name 
                                                        FROM PermissionSetAssignment 
                                                        WHERE Assignee.Id =:userinfo.getuserId()
                                                        and PermissionSet.Name = 'Salesforce CPQ Admin' ];
        
        if(permissioncheck.size() == 1)
            return true;
        else
            return false;
    }
    @AuraEnabled
    //Method for checking queued jobs
    public static List<AsyncApexJob> checkQueuedJobs() {
        return [SELECT Id,status FROM AsyncApexJob WHERE jobtype = 'Queueable' and status = 'Processing' and ApexClass.Name = 'GetApttusDataQueueable' WITH SECURITY_ENFORCED order by createddate desc ];
    }
    //Method for getting named credentials
    @AuraEnabled
    public static List<NamedCredential> getNamedCredentials() {
        return [SELECT DeveloperName,Endpoint,Id,MasterLabel,PrincipalType FROM NamedCredential WHERE CalloutOptionsGenerateAuthorizationHeader = true WITH SECURITY_ENFORCED];
    }
	//Method for getting errors
    @AuraEnabled
    public static List<Error_Master__c> getErrorData() {
       return [SELECT Id, Name, Error__c  FROM Error_Master__c WITH SECURITY_ENFORCED order by CreatedDate desc Limit 5];
    }
    //Method for getting status after retrieve
    @AuraEnabled
    public static String getDataRetrieveStatus() {
       
        String status = '';
        List<Bulk_Jobs_Status__c> bulkJobStatusLst = [SELECT Id, Status__c from Bulk_Jobs_Status__c WITH SECURITY_ENFORCED ORDER BY createddate desc LIMIT 2];
        if (!bulkJobStatusLst.isEmpty()) {
            status = bulkJobStatusLst[0].Status__c;
        }

        return status;
    }
	 //Method for setting bulkJobStatus
    @AuraEnabled
    public static boolean openBulkJobStatus() {
        boolean flag = false;
        List<Bulk_Jobs_Status__c> bulkJobStatusLst = new List<Bulk_Jobs_Status__c>();
        if (Bulk_Jobs_Status__c.SObjectType.getDescribe().isAccessible() && Schema.SObjectType.Bulk_Jobs_Status__c.fields.Status__c.isAccessible()) {
            bulkJobStatusLst = [SELECT id from Bulk_Jobs_Status__c WHERE Status__c = 'Open'];
            if(!bulkJobStatusLst.isEmpty()){
                flag = true;
            }
        }

        return flag;
    }
	//Method for initiating retrieval process
    @AuraEnabled
    public static boolean initiateRetrival(String selectedObjectMap,String selectednamedCredentials) {
        String selectedCred = selectednamedCredentials;
        List<String> selectedComponets = (List<String>)JSON.deserialize(selectedObjectMap, List<String>.class);
        for(String objectName : selectedComponets){
            if(objectName != 'ProductAttribute'){
                System.enqueueJob(new GetApttusDataQueueable('false',objectName,'',selectedCred));   
            }else{
                System.enqueueJob(new GetApttusDataQueueable('false','Apttus_Config2__ProductAttribute__c','',selectedCred));
                System.enqueueJob(new GetApttusDataQueueable('false','Apttus_Config2__ProductAttributeGroup__c','',selectedCred));
                System.enqueueJob(new GetApttusDataQueueable('false','Apttus_Config2__ProductAttributeGroupMember__c','',selectedCred));
            }
        }
        return true;
    }
    //Method to migrate data which is retrieved
    @AuraEnabled
    public static boolean pushDataToSFDCCPQ() {
        Database.executeBatch(new MigrationUtility_PushDataSFDCCPQ('Product2'),40);
        return true;
    }
    //Method for deleting data
    @AuraEnabled
    public static Boolean cleanupData(){
        Cleanup_Data_Utility.deleteStageData();
        return true;
    }
}