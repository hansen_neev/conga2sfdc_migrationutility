@isTest
public class Hansen_ProductMigrationBatchTest {
    @testSetup
    static void setup() {
    List<ProductToTransform__c> productsList=new List<ProductToTransform__c>();
        ProductToTransform__c p1=new ProductToTransform__c();
        ProductToTransform__c p2=new ProductToTransform__c();
        productsList.add(p1);
        productsList.add(p2);
        insert productsList;
    }
      
    @isTest
    public static void productMigrationBatchTest()
    {
        Hansen_ProductMigrationBatch  h=new Hansen_ProductMigrationBatch ();
        Test.startTest();
        Id batchId=Database.executeBatch(h);
        Test.stopTest();
        
    }

}