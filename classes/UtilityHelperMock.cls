global class UtilityHelperMock implements HttpCalloutMock {
     global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
     
         String data='{"id":200000000000000000000000000000000000000,JobComplete}';      
        res.setBody(data);
                res.setStatusCode(200);
        return res;
    
      
    }

}