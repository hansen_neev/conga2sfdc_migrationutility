public class APIResponse {
    public boolean hasErrors{get;set;}
    public List<ResponseData> results{get;set;}
}