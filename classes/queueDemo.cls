public class queueDemo implements Queueable {
	private integer cnt;
   
    public queueDemo(Integer cnt) {
       this.cnt = cnt;
    }

    public void execute(QueueableContext context) {
       	cnt = cnt + 1;
        
        if(cnt < 120){
            System.enqueueJob(new queueDemo(cnt));   
        }
    }
}