/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-10-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-03-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global class getDataSchedulable  implements Schedulable  {
    String JobID;
    String objectType;
    String bulkJobId;
    String nameCred;

   
    public getDataSchedulable(String JobID, String objectType){
        this.JobID = JobID;
        this.objectType = objectType; 
    }

     // Code added by Mehul Parmar with Bulk Job Id as Param - Start
     public getDataSchedulable(String JobID, String objectType,String bulkJobId){
        this.JobID = JobID;
        this.objectType = objectType; 
        this.bulkJobId = bulkJobId;   
    }

     // Code added by Mehul Parmar with Bulk Job Id as Param - Start
     public getDataSchedulable(String JobID, String objectType,String bulkJobId, String nameCred){
        this.JobID = JobID;
        this.objectType = objectType; 
        this.bulkJobId = bulkJobId;   
        this.nameCred = nameCred;   
    }
    
     // Code added by Mehul Parmar with Bulk Job Id as Param - End
    global void execute(SchedulableContext sc) {
        try{
            CronTrigger ct = [SELECT TimesTriggered, NextFireTime FROM CronTrigger WHERE id=:sc.getTriggerId()];
            System.abortJob(ct.Id);
            System.enqueueJob(new GetApttusDataQueueable('true',this.objectType,this.JobID,this.bulkJobId, this.nameCred));
        }catch(Exception ex){
            System.debug('Exception Occurred--> '+ex.getMessage()+'   '+ex.getStackTraceString());
            Error_master__c objEM = new Error_Master__c(name = ex.getTypeName(), Error__c=ex.getStackTraceString()+' '+ex.getMessage());
   			if (Schema.sObjectType.Error_master__c.fields.name.isUpdateable() && Schema.sObjectType.Error_master__c.fields.Error__c.isUpdateable()) {
                insert objEM;   
            }       
        }
   }
}