global class CreateJobRecords implements Schedulable {
    String JobName;
    String JobID;
    String JobStatus;
    public CreateJobRecords(String JobName,String JobID, String JobStatus){
        this.JobName = JobName;
        this.JobID = JobID;
        this.JobStatus = JobStatus;    
    } 
    global void execute(SchedulableContext sc) {
        CronTrigger ct = [SELECT TimesTriggered, NextFireTime FROM CronTrigger WHERE id=:sc.getTriggerId()];
        System.abortJob(ct.Id);
        Bulk_Jobs_Status__c objJobStatus = new Bulk_Jobs_Status__c();
        objJobStatus.Name = JobName;
        objJobStatus.Bulk_Job_ID__c = JobID;
        objJobStatus.Status__c = JobStatus;
        insert objJobStatus;
    }
}