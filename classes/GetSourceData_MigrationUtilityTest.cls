@isTest
public class GetSourceData_MigrationUtilityTest {
    @isTest
    public static  void migrationUtilityTest()
    {
        List<Source_to_Staging__c> liMappingData=new  List<Source_to_Staging__c>() ;
        Source_to_Staging__c m2=new Source_to_Staging__c();
        m2.Object_Type__c='Product2';
        Source_to_Staging__c m3=new Source_to_Staging__c();
        m3.Object_Type__c=' Apttus_Config2__ClassificationHierarchy__c';
        Source_to_Staging__c m4=new Source_to_Staging__c();
        m4.Object_Type__c=' Apttus_Config2__Pricelist__c';
        Source_to_Staging__c m5=new Source_to_Staging__c();
        m5.Object_Type__c=' Apttus_Config2__PriceListItem__c';
        Source_to_Staging__c m6=new Source_to_Staging__c();
        m6.Object_Type__c=' Apttus_Config2__ProductOptionGroup__c';
        Source_to_Staging__c m7=new Source_to_Staging__c();
        m7.Object_Type__c=' Apttus_Config2__ProductOptionComponent__c';
        Source_to_Staging__c m8=new Source_to_Staging__c();
        m8.Object_Type__c='Apttus_Config2__ProductAttributeGroupMember__c';
        Source_to_Staging__c m9=new Source_to_Staging__c();
        m9.Object_Type__c=' Apttus_Config2__ProductAttributeGroup__c';
       	Source_to_Staging__c m10=new Source_to_Staging__c();
        m10.Object_Type__c=' Apttus_Config2__ProductAttribute__c';
       
        liMappingData.add(m2);
        liMappingData.add(m3);
        liMappingData.add(m4);
        liMappingData.add(m5);
        liMappingData.add(m6);
        liMappingData.add(m7);
        liMappingData.add(m8);
        liMappingData.add(m9);
        liMappingData.add(m10);
       	insert liMappingData;
       	GetSourceData_MigrationUtility  g=new  GetSourceData_MigrationUtility ('h','10','','10','devApttusApi');
        GetSourceData_MigrationUtility  g2=new  GetSourceData_MigrationUtility ('h','','','10','devApttusApi');
        g.liMappingData=liMappingData;
        g.JobID='10';
        g.pollingMode='1';
        g.bulkJobId='';
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MigrationUtilityTestHttpMock());
     	integer p=g.initiateSync();
        Test.stopTest();
    }

    @isTest
    public static  void migrationUtilityTestWithoutException()
    {
        List<Source_to_Staging__c> liMappingData=new  List<Source_to_Staging__c>() ;
        GetSourceData_MigrationUtility  g=new  GetSourceData_MigrationUtility ('Apttus_Config2__ClassificationHierarchy__c','true','','');
        GetSourceData_MigrationUtility  g2=new  GetSourceData_MigrationUtility ('h','','','');
        GetSourceData_MigrationUtility  g3=new  GetSourceData_MigrationUtility ('Apttus_Config2__Pricelist__c','true','','');
        GetSourceData_MigrationUtility  g4=new  GetSourceData_MigrationUtility ('Apttus_Config2__ProductAttributeGroupMember__c','true','','');
        GetSourceData_MigrationUtility  g5=new  GetSourceData_MigrationUtility ('INVALIDJOB','true','','');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MigrationUtilityTestHttpMock());
      	integer p=g.initiateSync();
        Test.stopTest();
    }

}