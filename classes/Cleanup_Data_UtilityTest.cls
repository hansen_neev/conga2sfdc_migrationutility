/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-10-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-10-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private with sharing class Cleanup_Data_UtilityTest {
    
	//test method for delete Stage data
    @isTest 
    static void testDeleteStageData(){
        Stage_Product__c oSP = TestDataFactory.getStageProduct();
        insert oSP;
		Stage_Pricing__c prp = TestDataFactory.getStagePricing();
        insert prp;
		Stage_Attribute__c pap=TestDataFactory.getStageAttribute();
        insert pap;
        Error_Master__c erp=TestDataFactory.getErrorMaster();
        insert erp;
        Bulk_Jobs_Status__c bjs=TestDataFactory.getBulkJobStatus();
        insert bjs;
        Cleanup_Data_Utility.deleteStageData();

        List<Stage_Product__c> liStageProduct = [select Id from Stage_Product__c];
        System.assertEquals(liStageProduct.isEmpty(), true);
        List<Stage_Pricing__c> liStagePricing = [Select id from Stage_Pricing__c];
        System.assertEquals(liStagePricing.isEmpty(), true);
        
        List<Stage_Attribute__c> liStageAttribute = [Select Id from Stage_Attribute__c];
        System.assertEquals(liStageAttribute.isEmpty(), true);
        
        List<Error_Master__c> liErrorMaster = [Select Id from Error_Master__C];
        System.assertEquals(liErrorMaster.isEmpty(), true);
        
        List<Bulk_Jobs_Status__c> liBulkStatus = [select Id from Bulk_Jobs_Status__c ];
        System.assertEquals(liBulkStatus.isEmpty(), true); 
    }
    //test method for deleteTargetData
    @isTest 
    static void testDeleteTargetData(){
        Pricebook2 pc=TestDataFactory.getPricebook2();
        insert pc;
        Cleanup_Data_Utility.deleteTargetData();
        List<Pricebook2> liPricebook = [select Id from Pricebook2 WHERE ExternalId__c !=null];
        System.assertEquals(liPricebook.isEmpty(), true);
         
        List<PricebookEntry> liPriceBookEntry = [SELECT ID from PricebookEntry where pricebook2.isStandard = false or Product2.External_Id__c != null];
        System.assertEquals(liPriceBookEntry.isEmpty(), true);
         
        List<Product2> liProducts = [select Id from product2 WHERE External_Id__c !=null];
        System.assertEquals(liProducts.isEmpty(), true);
            
        List<SBQQ__ProductFeature__c> liPFeature = [select Id from SBQQ__ProductFeature__c  WHERE ExternalId__c !=null];
        System.assertEquals(liPFeature.isEmpty(), true);
          
        List<SBQQ__ProductOption__c> liPOption = [select Id from SBQQ__ProductOption__c WHERE ExternalId__c !=null];
        System.assertEquals(liPOption.isEmpty(), true);
        
        
        
        List<SBQQ__AttributeSet__c> liAttributeSet = [select Id from SBQQ__AttributeSet__c  WHERE ExternalId__c !=null];
        System.assertEquals(liAttributeSet.isEmpty(), true);
        
        List<SBQQ__ProductAttributeSet__c> liProductAttributeSet = [select Id from SBQQ__ProductAttributeSet__c];
        System.assertEquals(liProductAttributeSet.isEmpty(), true);
            
        List<SBQQ__ProductAttribute__c> liPA = [select Id from SBQQ__ProductAttribute__c ];
        System.assertEquals(liPA.isEmpty(), true);
        
        List<SBQQ__ConfigurationAttribute__c> liCA = [select Id from SBQQ__ConfigurationAttribute__c WHERE ExternalId__c !=null];
        System.assertEquals(liCA.isEmpty(), true);
       }
    //test method for delete all data
     @isTest 
    static void testDeleteAllData_test(){
         Cleanup_Data_Utility.deleteAllData();
}
}