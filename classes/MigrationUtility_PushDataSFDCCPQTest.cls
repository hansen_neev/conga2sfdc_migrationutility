@isTest
public class MigrationUtility_PushDataSFDCCPQTest {
    
    @TestSetup
    static void makeData(){
        List<sObject> ls = Test.loadData(Source_to_Staging__c.sObjectType, 'sourceToStaging');
        List<sObject> ls2 = Test.loadData(Staging_to_CPQ__c.sObjectType, 'StagingToCpq');
     
        List<Stage_Product__c> listObjects = new List<Stage_Product__c>();
        Stage_Product__c record1 = new Stage_Product__c();
        record1.Product2_Active__c=true;
        record1.Product2_Name__c='SomeName';
        record1.Product2_Option_Group_Category_Hierarchy__c='something';
        record1.Product2_Customizable__c=true;
        record1.Product2_Has_Attributes__c=true;
        record1.Product2_Has_Option__c=true;
        record1.Product2_Configuration_Type__c='Option';
        record1.Product2_Show_tab_view__c=true;

        Stage_Product__c record2 = new Stage_Product__c();
        record2.Product2_Active__c=true;
        record2.Product2_Name__c='SomeName';
        record2.Product2_Option_Group_Category_Hierarchy__c='something';
        record2.Product2_Customizable__c=false;
        record2.Product2_Has_Attributes__c=true;
        record2.Product2_Has_Option__c=true;
        record2.Product2_Configuration_Type__c='other';
   
        listObjects.add(record2);
        listObjects.add(record1);
        insert listObjects;
		Stage_Attribute__c scc=new Stage_Attribute__c();
        scc.Attribute_Group_Id__c='someId';
        scc.ExternalID__c='someId';
        insert scc;
         
		Product2  p=new Product2();
        p.External_Id__c='pId2';
        p.name='test';
        insert p;
		Staging_to_CPQ__c s=new Staging_to_CPQ__c();
        s.Object_Type__c='Product2';
         s.Source_field__c='Product2_Family__c';
         s.Target_Field__c='Family';
		insert s;        
		SBQQ__ProductOption__c scw=new SBQQ__ProductOption__c();
		scw.SBQQ__Number__c=2.0;
        insert scw;
        Product2 prod = new Product2(Name = 'Laptop X200', Family = 'Hardware',External_Id__c='plip');
        insert prod;
        Product2 prod2 = new Product2(Name = 'PC X200',Family = 'Hardware',External_Id__c='pID');
        insert prod2;	
        Product2 prod3 = new Product2(Name = 'PC X2000', Family = 'Hardware',External_Id__c='plip2');
        insert prod3;
        Id pricebookId = Test.getStandardPricebookId();
        
        
        //  Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,
        UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        Pricebook2 customPB1 = new Pricebook2(Name='Custom Pricebook', isActive=true);
        customPB1.ExternalID__c='someName2';
        
        insert customPB1;
       
        // 2. Insert a price book entry with a custom price.
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
       	customPB.ExternalID__c='plip';
        
        insert customPB;
        
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
        Pricebook2Id = customPB.Id, Product2Id = prod.Id,
        UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        Pricebook2 standardPricebook = new Pricebook2(
        Id = Test.getStandardPricebookId(),
        IsActive = true
        );
        //Execute an update DML on the Pricebook2 record, to make IsStandard to true
        Update standardPricebook;
		
        
   
        Stage_Product__c sp=new Stage_Product__c(Product2_Option_Group_Category_Hierarchy__c='something',ExternalId__c='pId',OptionComponent_Component_Product__c='Apttus_Config2__ProductOptionComponent__c');
        Stage_Product__c sp2=new Stage_Product__c(Product2_Option_Group_Category_Hierarchy__c='something',ExternalId__c='pId',OptionGroup_Product__c='Apttus_Config2__ProductOptionGroup__c',Product2_Customizable__c=false,Product2_Has_Attributes__c=true,Product2_Has_Option__c=true,Product2_Configuration_Type__c='Option');
        Stage_Product__c sp3=new Stage_Product__c(ExternalId__c='pID',Product2_Option_Group_Category_Hierarchy__c ='Apttus_Config2__ClassificationHierarchy__c',Product2_Customizable__c=true,Product2_Has_Attributes__c=true,Product2_Has_Option__c=true,Product2_Configuration_Type__c='Option');
        List<Stage_Product__c> spList=new List<Stage_Product__c>();
        spList.add(sp);
        spList.add(sp2);
        spList.add(sp3);
        insert spList;
		Stage_Pricing__c stagePricing2=new Stage_Pricing__c(Price_List_Name__c='so',Price_List_Item_Pricelist__c='someName2',Based_on_Price_List__c='someName2',Price_List_Item_Pricemethod__c='Percentage',Price_List_Item_Product__c='test');
        stagePricing2.ExternalID__c='someName2';
        List<Stage_Pricing__c> stagePricingList=new List<Stage_Pricing__c>();
        stagePricingList.add(stagePricing2);
        insert stagePricingList;


        Stage_Attribute__c sa=new Stage_Attribute__c(Attribute_Group_Id__c='someId',Product_Id__c='pId');
        Stage_Attribute__c sa2=new Stage_Attribute__c(Attribute_Group_Id__c='',Product_Id__c='',Attribute_Target_Field__c='');
        insert sa2;
        insert sa;

        SBQQ__ProductFeature__c pf=new SBQQ__ProductFeature__c(SBQQ__Number__c=12,Name='good',ExternalId__c='pID');
        insert pf;
        Stage_Attribute__c sc=new Stage_Attribute__c();
        sc.Attribute_Group_Id__c ='someId';
        insert sc;
        SBQQ__AttributeSet__c atSet=new SBQQ__AttributeSet__c(ExternalId__c='pID');

        insert atSet;
        
    }
    
    
    @isTest
    public static void testMigrationUtilityForProduct2()
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('Product2');
        Test.startTest();
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }
    @isTest
    public static void testMigrationUtilityForSBQQProductFeature()
    {
        
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('SBQQ__ProductFeature__c');
        Test.startTest();
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }
    @isTest
    public static void testMigrationUtilityForSBQQProductOption()
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('SBQQ__ProductOption__c');
       Test.startTest();
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }

    @isTest
    public static void testMigrationUtilityForPricebook2()
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('Pricebook2');
        Test.startTest();
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }

    @isTest
    public static void testMigrationUtilityForSBQQConfigurationAttribute()
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('SBQQ__ConfigurationAttribute__c');
        Test.startTest();
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }

    @isTest
    public static void testMigrationUtilityForPricebookEntry()    // on 466 line insert fails as entry required entry is not provided 
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('PricebookEntry');
        Test.startTest();
        Stage_Pricing__c stagePricing=new Stage_Pricing__c(Price_List_Name__c='',Price_List_Item_Product__c='plip',Price_List_Item_Pricelist__c='someName2',Based_on_Price_List__c='basedPL');
       
        insert stagePricing;
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }
@isTest
    public static void testMigrationUtilityForPricebookEntry2()    // to make 484 line condition but made 455 true
    {
        MigrationUtility_PushDataSFDCCPQ mup=new MigrationUtility_PushDataSFDCCPQ('PricebookEntry');
        Test.startTest();
        Stage_Pricing__c stagePricing=new Stage_Pricing__c(Price_List_Name__c='',Price_List_Item_Product__c='plip2',Price_List_Item_Pricelist__c='someName2',Based_on_Price_List__c='basedPL');
        insert stagePricing;
        Id batchId = Database.executeBatch(mup);
        Test.stopTest();
    }

    
        
}