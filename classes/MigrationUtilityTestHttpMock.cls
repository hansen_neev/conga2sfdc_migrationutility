global class MigrationUtilityTestHttpMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String data='JobComplete0000000000000000000000000000000000000000000000000000';      
        res.setBody(data);
                res.setStatusCode(200);
        return res;
    
      
    }
}