@isTest
public inherited sharing class MetadataRetrieveControllerTest {
    
    @isTest
    public static void initTest()
    {
        Test.setMock(WebServiceMock.class, new MetadataServiceDescribeMetadataMock());
        Test.startTest();
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.init();
        Test.stopTest();
        
    }   

    @isTest
    public static void listMetadataItemsTest()
    {
        Test.setMock(WebServiceMock.class, new MetadataServiceListMetadataMock());
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.listMetadataItems();
    }

    @isTest
    public static void retrieveMetadataItemTest()
    {
        Test.setMock(WebServiceMock.class, new MetadataServiceRetrieveMetadataItemMock());
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.retrieveMetadataItem();
    }

    @isTest
    public static void checkAsyncRequestTest()
    {
        Test.setMock(WebServiceMock.class, new MetadataServiceRetrieveMetadataItemMock());
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.retrieveMetadataItem();
        Test.setMock(WebServiceMock.class, new MetadataAsyncRequestTestMock());
       metadata.checkAsyncRequest();
    }

    @isTest
    public static void receiveMetadataZipFileTest()
    {
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.MetadataFileName='someNameOfZipFile';
        metadata.MetaDataFileData='Data in file to be zipped';
        Test.setMock(WebServiceMock.class, new MetadataServiceRetrieveMetadataItemMock());
        metadata.retrieveMetadataItem();
        Test.setMock(WebServiceMock.class, new MetadataAsyncRequestTestMock());
       metadata.checkAsyncRequest();
        metadata.receiveMetadataZipFile();
    }

    @isTest
    public static void receiveCompleteTest()
    {
        MetadataRetrieveController metadata=new MetadataRetrieveController();
        metadata.receiveComplete();
    }

    @isTest
    public static void MetadataFileTest()
    {
        Test.startTest();
        MetadataRetrieveController.MetadataFile mf=new MetadataRetrieveController.MetadataFile();
        mf.getContent();
        mf.getFullname();
        Test.stopTest();
    }

    
}