public class GetSourceData_MigrationUtility {
    
    public Map<String,String> objectQuery = new Map<String,String>();
    public String jobCompletionStatus = 'JobComplete';
    public  Map<String,Map<String,String>> fieldsHeaderToReplace = new Map<String,Map<String,String>>();
    public  List<Source_to_Staging__c> liMappingData ;
    public  string CurrentobjectType = '';
    public String JobID;
    public String pollingMode;
    public String bulkJobId;
    public String nameCred ='';
    
    public GetSourceData_MigrationUtility(String CurrentobjectType,String pollingMode, String JobID,String bulkJobId,String nameCred){
        this.CurrentobjectType = CurrentobjectType ;
        this.JobID = JobID;
        this.pollingMode = pollingMode;
        this.bulkJobId = bulkJobId;
        this.nameCred = nameCred;
        populateAllMaps();
    }
    
    public GetSourceData_MigrationUtility(String CurrentobjectType,String pollingMode, String JobID,String nameCred){
        this.CurrentobjectType = CurrentobjectType ;
        this.JobID = JobID;
        this.pollingMode = pollingMode;
        this.nameCred = nameCred;
        populateAllMaps();
    }
    
	//Method for populating queryMap and HeaderToReplaceMap 
    private void populateAllMaps(){
        populateHeaderToReplaceMap();
        populateObjectQueryMap();
    }
    // Creating queries and populating in map
    private void populateObjectQueryMap(){
        String ProductFieldsCommaSeparatedValue ='';
        String ProductOGFieldsCommaSeparatedValue ='';
        for(Source_to_Staging__c objMapping : liMappingData){
            if(this.CurrentobjectType.equalsIgnoreCase(objMapping.Object_Type__c)){
                if(!ProductFieldsCommaSeparatedValue.contains(objMapping.Source_Field__c+',')){
                    ProductFieldsCommaSeparatedValue += objMapping.Source_Field__c + ',';    
                }
            }
        }
        ProductFieldsCommaSeparatedValue = ProductFieldsCommaSeparatedValue.removeEnd(',');
        objectQuery.put(this.CurrentobjectType,'SELECT '+ProductFieldsCommaSeparatedValue+' from '+this.CurrentobjectType);
    }
    
    private void populateHeaderToReplaceMap(){
        Map<String,String> productFieldToReplace = new Map<String,String>();
        Map<String,String> productOpGroupFieldToReplace = new Map<String,String>();
        Map<String,String> productOpComponentFieldToReplace = new Map<String,String>();
        Map<String,String> productClassificationName = new Map<String,String>();
        Map<String,String> priceListItemFieldToReplace = new Map<String,String>();
        Map<String,String> priceListFieldToReplace = new Map<String,String>();
        
        
        //For Attribute
        Map<String,String> ProductAttributeGroupMember = new Map<String,String>();
        Map<String,String> ProductAttributeGroup = new Map<String,String>();
        Map<String,String> AttributeMember = new Map<String,String>();
        
        liMappingData = [Select Id,Name,Object_Type__c, Source_Field__c, Target_Field__c from Source_to_Staging__c];
        
        for(Source_to_Staging__c objMapping : liMappingData){
            if(objMapping.Object_Type__c.equalsIgnoreCase('Product2')){
                productFieldToReplace.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ClassificationHierarchy__c')){
                productClassificationName.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__Pricelist__c')){
                priceListFieldToReplace.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__PriceListItem__c')){
                priceListItemFieldToReplace.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ProductOptionGroup__c')){
                productOpGroupFieldToReplace.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ProductOptionComponent__c')){
                productOpComponentFieldToReplace.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            
            //For Attribute
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ProductAttributeGroupMember__c')){
                ProductAttributeGroupMember.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ProductAttributeGroup__c')){
                ProductAttributeGroup.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
            if(objMapping.Object_Type__c.equalsIgnoreCase('Apttus_Config2__ProductAttribute__c')){
                AttributeMember.put(objMapping.Source_Field__c , objMapping.Target_Field__c);            
            }
        }
        fieldsHeaderToReplace.put('Product2',productFieldToReplace);
        fieldsHeaderToReplace.put('Apttus_Config2__ProductOptionGroup__c',productOpGroupFieldToReplace);
        fieldsHeaderToReplace.put('Apttus_Config2__ProductOptionComponent__c',productOpComponentFieldToReplace);
        fieldsHeaderToReplace.put('Apttus_Config2__Pricelist__c',priceListFieldToReplace);
        fieldsHeaderToReplace.put('Apttus_Config2__ClassificationHierarchy__c',productClassificationName);
        fieldsHeaderToReplace.put('Apttus_Config2__PriceListItem__c',priceListItemFieldToReplace);
        
        //Attribute
        fieldsHeaderToReplace.put('Apttus_Config2__ProductAttributeGroupMember__c',ProductAttributeGroupMember);
        fieldsHeaderToReplace.put('Apttus_Config2__ProductAttributeGroup__c',ProductAttributeGroup);
        fieldsHeaderToReplace.put('Apttus_Config2__ProductAttribute__c',AttributeMember);
        
    }
    
    // Code added by Mehul Parmar with Bulk Job Id as Param - Start
   private void ScheduleJobAfter15Sec(String JobID,String JobStatusRecordID,String namedCred ){
        Datetime timeafter15Seconds = Datetime.now().addSeconds(10) ;
        String hour = String.valueOf(timeafter15Seconds.hour());
        String min = String.valueOf(timeafter15Seconds.minute());
        String ss = String.valueOf(timeafter15Seconds.second());
        String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?'; 
        getDataSchedulable scheduler = new getDataSchedulable(JobID,this.CurrentobjectType,JobStatusRecordID,nameCred);
        System.schedule('Get '+this.CurrentobjectType+' Data', nextFireTime, scheduler);
   }
    
    // Code for sending request for BulkQueryJob
    public integer initiateSync(){
        
        try{
            List<Bulk_Jobs_Status__c> liBulkJobStatusJobCompleted = new List<Bulk_Jobs_Status__c>();
            List<Bulk_Jobs_Status__c> liBulkJobStatusJobOpen = new List<Bulk_Jobs_Status__c>();
            String objectName = this.CurrentobjectType ;
            String nameCreds = this.nameCred ;
            String bulkQueryJobId ;
            
            if(this.pollingMode == 'true'){
                bulkQueryJobId = this.JobID;
            }else{
                bulkQueryJobId = SourceData_to_Staging_UtilityHelper.initiateBulkQueryJob(objectName,objectQuery.get(objectName),nameCreds);
                liBulkJobStatusJobOpen.add(new Bulk_Jobs_Status__c(Name='InitiateBulkQuery '+objectName+' '+Integer.valueof((Math.random() * 1000)), Status__c = 'Open', Bulk_Job_ID__c = bulkQueryJobId));
            }
          
            boolean jobStatus = SourceData_to_Staging_UtilityHelper.pollTillJobComplete(bulkQueryJobId,nameCreds);
            if(jobStatus){
                String jobResponse;
                   jobResponse = SourceData_to_Staging_UtilityHelper.getJobResponse(bulkQueryJobId,nameCreds);
                if(jobResponse != '' && jobResponse != null){
                    String reFormedResponse = SourceData_to_Staging_UtilityHelper.getReformedResponse(fieldsHeaderToReplace.get(objectName),jobResponse);
                    String jobId ;
                    if(objectName == 'Apttus_Config2__ClassificationHierarchy__c' || objectName == 'Product2' || objectName == 'Apttus_Config2__ProductOptionGroup__c' || objectName == 'Apttus_Config2__ProductOptionComponent__c'){
                        jobId = SourceData_to_Staging_UtilityHelper.createJobForInsert('Stage_Product__c');
                        liBulkJobStatusJobCompleted.add(new Bulk_Jobs_Status__c(Id=this.bulkJobId,Name='CreateBulkJobforinsert'+objectName+' '+Integer.valueof((Math.random() * 1000)), Status__c = 'Completed', Bulk_Job_ID__c = jobId));
                    }
                    
                    if(objectName == 'Apttus_Config2__Pricelist__c' || objectName == 'Apttus_Config2__PriceListItem__c'){
                        jobId = SourceData_to_Staging_UtilityHelper.createJobForInsert('Stage_Pricing__c');
                        liBulkJobStatusJobCompleted.add(new Bulk_Jobs_Status__c(Id=this.bulkJobId,Name='CreateBulkJobforinsert'+objectName+' '+Integer.valueof((Math.random() * 1000)), Status__c = 'Completed', Bulk_Job_ID__c = jobId));
                    }
                    
                    if(objectName == 'Apttus_Config2__ProductAttributeGroupMember__c' || objectName == 'Apttus_Config2__ProductAttributeGroup__c' || objectName == 'Apttus_Config2__ProductAttribute__c'){
                        jobId = SourceData_to_Staging_UtilityHelper.createJobForInsert('Stage_Attribute__c');
                        liBulkJobStatusJobCompleted.add(new Bulk_Jobs_Status__c(Id=this.bulkJobId,Name='CreateBulkJobforinsert'+objectName+' '+Integer.valueof((Math.random() * 1000)), Status__c = 'Completed', Bulk_Job_ID__c = jobId));
                    }
                    
                    if(jobId!= 'INVALIDJOB' && jobId != '' && String.isNotBlank(jobId)){
                        boolean flag = SourceData_to_Staging_UtilityHelper.pushDataToJob(jobId,reFormedResponse);
                        if(flag){
                            boolean jobClosed = SourceData_to_Staging_UtilityHelper.closeJob(jobId);                
                        }else{
                            //Error Handling Mechanism
                        }
                    }
                    
                }
                if(liBulkJobStatusJobCompleted.size() >0){
                  if (Schema.sObjectType.Bulk_Jobs_Status__c.fields.Name.isUpdateable() && Schema.sObjectType.Bulk_Jobs_Status__c.fields.Status__c.isUpdateable() && Schema.sObjectType.Bulk_Jobs_Status__c.fields.Bulk_Job_ID__c.isUpdateable()){ 
                    	
                    upsert liBulkJobStatusJobCompleted;
                  }
                }
                return 1;
            }else{
                if(liBulkJobStatusJobOpen.size() >0){
                    
                     if (Schema.sObjectType.Bulk_Jobs_Status__c.fields.Name.isUpdateable() && Schema.sObjectType.Bulk_Jobs_Status__c.fields.Status__c.isUpdateable() && Schema.sObjectType.Bulk_Jobs_Status__c.fields.Bulk_Job_ID__c.isUpdateable()){ 
               			System.debug('bulkJobStatus'+liBulkJobStatusJobOpen);
                    	insert liBulkJobStatusJobOpen;
                     }
                }
                if(liBulkJobStatusJobOpen !=null && liBulkJobStatusJobOpen.size() >0){
                    ScheduleJobAfter15Sec(bulkQueryJobId,liBulkJobStatusJobOpen[0].Id,this.nameCred);
                }
                return 0;
            }
        }catch(Exception utilityException){
            Error_master__c objEM = new Error_Master__c(Name =utilityException.getTypeName(),Error__c=utilityException.getMessage()+' '+utilityException.getStackTraceString());
            if (Schema.sObjectType.Error_master__c.fields.Id.isUpdateable() && Schema.sObjectType.Error_master__c.fields.Error__c.isUpdateable()){ 
             	insert objEM;
            }
           return 0;
        }
    } 
}