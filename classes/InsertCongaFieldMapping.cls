public with sharing  class InsertCongaFieldMapping {
//Method For deleting existing objects of Source_to_Staging__c
	private static  void deleteSourceToStaging()
    {
        try{
       		if (Schema.sObjectType.Source_to_Staging__c.isDeletable()) { 
        		delete [select Id from Source_to_Staging__c WITH SECURITY_ENFORCED ];
         }
        }catch(Exception e){
           	System.debug('ex ' + e.getMessage());
            throw e;
        }
    }
//Method For deleting existing objects of Staging_to_CPQ__c
    private static void deleteStagingToCPQ()
    {
    try{
    		if (Schema.sObjectType.Staging_to_CPQ__c.isDeletable()) { 
       			delete [select Id from Staging_to_CPQ__c WITH SECURITY_ENFORCED];
         }
    }catch(Exception e){
            System.debug('ex ' + e.getMessage());
            throw e;
    }
   }
    // Read static resource csv
    private static List<String> staticResourceBodyLines(String p,Map<String,StaticResource> staticResourceMap)
    {
       //StaticResource staticResource = [SELECT Id,Body FROM StaticResource WHERE Name =:p LIMIT 1];
       List<String> lines = staticResourceMap.get(p).Body.toString().split('\n');
        return lines;
    }
    private  static List<String> getFieldList(String field)
    {
        List<String > fieldList=new List<String>();
        for(string st:field.split(','))
        {
           fieldList.add(st); 
        }
     return fieldList;
    }
    //Parse CSV to get Source To Staging Objects
    private static List<Source_to_Staging__c> getSourceToStagingObjects(List<String> lines,List<String> fieldList)
    {
        List<Source_to_Staging__c> sourceToStagingObjects=new List<Source_to_Staging__c> ();
        for(Integer i=1;i<lines.size();i++){
           
           Source_to_Staging__c sourceToStaging = new  Source_to_Staging__c() ;
           List<String> data = lines[i].split(',');
           sourceToStaging.Is_Direct_Mapping__c =boolean.valueOf(data[1]);           
           sourceToStaging.Object_Type__c =data[4];
           sourceToStaging.Source_Field__c = data[5];
           sourceToStaging.Target_Field__c = data[6];                                                                             
           sourceToStagingObjects.add(sourceToStaging); 
       
       }
        return sourceToStagingObjects;
    }
    //Parse CSV to get Staging To CPQ Objects
  private static List<Staging_to_CPQ__c> getStagingToCpqObjects(List<String> Lines,List<String> fieldList)
    {
   
     List<Staging_to_CPQ__c> stagingToCpqObjects=new List<Staging_to_CPQ__c>();
       for(Integer i=1;i<lines.size();i++){
        Staging_to_CPQ__c stagingToCpq = new  Staging_to_CPQ__c() ;
         List<String> data = lines[i].split(',');
         	stagingToCpq.Object_Type__c = data[3];
           	stagingToCpq.Source_Field__c = data[4];
           	stagingToCpq.Target_Field__c =data[5];                                                                             
          	stagingToCpqObjects.add(stagingToCpq); 
       
       }
        return stagingToCpqObjects;
    }   
    //Load Static Resouces in Map
    public static Map<String,StaticResource> loadStaticResources() 
    {	   
        Map<String,StaticResource> staticResourceMap=new Map<String,StaticResource>();
		StaticResource staticResource = [SELECT Id,Body FROM StaticResource WHERE Name ='sourceToStaging' WITH SECURITY_ENFORCED LIMIT 1];
        staticResourceMap.put('sourceToStaging',staticResource);
        staticResource = [SELECT Id,Body FROM StaticResource WHERE Name ='StagingToCpq' WITH SECURITY_ENFORCED LIMIT 1];
         staticResourceMap.put('StagingToCpq',staticResource);
        return staticResourceMap;
    }
    //Insert Objects of Source To Staging 
    public static void insertSourceToStaging(Map<String,StaticResource> staticResourceMap) 
    {
     try{
        List<String> lines=staticResourceBodyLines('sourceToStaging',staticResourceMap);
		List<String> fieldList=getFieldList(lines[0]);
        List<Source_to_Staging__c> sourceToStagingObjects=getSourceToStagingObjects(lines,fieldList);
			insert sourceToStagingObjects;
           }catch(Exception e)
        {
           System.debug('ex ' + e.getMessage());
            throw e;
        }
    }
    //Insert Objects of Staging To CPQ
    public static void insertStagingToCpq(Map<String,StaticResource> staticResourceMap)
    {
        try{
        List<String> lines=staticResourceBodyLines('StagingToCpq',staticResourceMap);
		List<String> fieldList=getFieldList(lines[0]);
        List<Staging_to_CPQ__c> stagingToCpqObjects=getStagingToCpqObjects(lines,fieldList);
		
    		insert  stagingToCpqObjects;
        }catch(Exception e)
        {
            System.debug('ex ' + e.getMessage());
            throw e;
        }
        
 	}
    //Method For Delete All Data
     public static void deleteAllData()
   {
       try{
       	deleteSourceToStaging();
        deleteStagingToCPQ();
       }catch(Exception e) {
           	System.debug('ex ' + e.getMessage());
            throw e;
       }
   }

   
}