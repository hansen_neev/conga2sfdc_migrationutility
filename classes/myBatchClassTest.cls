@isTest
public inherited sharing class myBatchClassTest {
    @TestSetup
    static void makeData(){
        List<SBQQ__SummaryVariable__c> summary=new List<SBQQ__SummaryVariable__c>();
        for(Integer i=0;i<5;i++)
        summary.add(new SBQQ__SummaryVariable__c(Name='someName'+i));

        insert summary;
    }

    @isTest
    public static void testBatchClass()
    {
        Test.startTest();
        myBatchClass bc=new myBatchClass('');
        Id batchId = Database.executeBatch(bc);
        Test.stopTest();
    }
}
