public class ProductMigration{

    String Name;
    Boolean isActive;
    AttributeWrapper attributes;
    public ProductMigration(AttributeWrapper wrap, ProductToTransform__c productStandalone){
        this.Name = productStandalone.Product_Name__c;
        this.isActive = productStandalone.Is_Active__c;  
        this.attributes = wrap;
    }
}